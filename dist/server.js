'use strict';

var _app = require('./app');

var _app2 = _interopRequireDefault(_app);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const { analytics } = _app2.default;

analytics.viewChangeSeat({ attr1: 'attribute1', attr2: 'attr2' });
analytics.cancelChangeSeat({ attr1: 'attribute1', attr2: 'attr2' });
analytics.confirmChangeSeat({ attr1: 'attribute1', attr2: 'attr2' });