'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
const config = {
  Checkin: {
    viewChangeSeat: {
      action: 'onValidate',
      points: 30,
      categories: ['Manage you booking', 'Checkin', 'Enter passenger details']
    },
    cancelChangeSeat: {
      action: 'onSubmit',
      points: 30,
      categories: ['Manage you booking', 'Checkin', 'Enter passenger details']
    },
    confirmChangeSeat: {
      action: 'onClick',
      points: 30,
      categories: ['Manage you booking', 'Checkin', 'Enter passenger details']
    }
  }
};

exports.default = config;