'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _lodash = require('lodash');

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _adobe = require('./provider/adobe');

var _adobe2 = _interopRequireDefault(_adobe);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const eventReducer = ({ module, events, provider }) => (0, _lodash.reduce)(events, (acc, props, name) => Object.assign({}, acc, {
  [name]: attributes => provider.send({
    name: `${module}_${name}_event`,
    module,
    action: props.action,
    categories: props.categories,
    timestamp: '',
    attributes
  })
}), {});

const createProvider = config => provider => (0, _lodash.reduce)(config, (acc, events, module) => Object.assign({}, acc, eventReducer({ module, events, provider })), {});

exports.default = createProvider(_config2.default)(_adobe2.default);