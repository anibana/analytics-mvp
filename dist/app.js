'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _analytics = require('./analytics');

var _analytics2 = _interopRequireDefault(_analytics);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const middlewares = {
  analytics: _analytics2.default
};

exports.default = middlewares;