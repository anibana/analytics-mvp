import {reduce} from 'lodash';
import config from './config';
import adobe from './provider/adobe';

const eventReducer = ({module, events, provider}) => (
  reduce(events, (acc, props, name) => ({
    ...acc,
    [name]: (attributes) => (
      provider.send({
        name: `${module}_${name}_event`,
        module,
        action: props.action,
        categories: props.categories,
        timestamp: '',
        attributes
      })
    )
  }), {})
);

const createProvider = (config) => (provider) => (
  reduce(config, (acc, events, module) => ({
    ...acc,
    ...eventReducer({module, events, provider})
  }), {})
);

export default createProvider(config)(adobe);
