const config = {
  Checkin: {
    viewChangeSeat: {
      action: 'onValidate',
      points: 30,
      categories: ['Manage you booking', 'Checkin', 'Enter passenger details'],
    },
    cancelChangeSeat: {
      action: 'onSubmit',
      points: 30,
      categories: ['Manage you booking', 'Checkin', 'Enter passenger details'],
    },
    confirmChangeSeat: {
      action: 'onClick',
      points: 30,
      categories: ['Manage you booking', 'Checkin', 'Enter passenger details'],
    }
  }
}

export default config;
