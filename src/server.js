import middlewares from './app';

const {analytics} = middlewares;

analytics.viewChangeSeat({attr1: 'attribute1', attr2: 'attr2'});
analytics.cancelChangeSeat({attr1: 'attribute1', attr2: 'attr2'});
analytics.confirmChangeSeat({attr1: 'attribute1', attr2: 'attr2'});
