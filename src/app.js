import analytics from './analytics';

const middlewares = {
  analytics
};

export default middlewares;

